package com.nmf;
import java.util.*;

public class PSO
{
	public static final int MAX = 100;
    public static final int c1 = 2;
    public static final int c2 = 2;
    public static final int a = 0;
    public static final int b = 30;
    public static final int w = 1;
	static Scanner sc=new Scanner(System.in);
    public static double[][] intialize(double[][] xij, double[][] vij, double[][] xijcap, double[] gcap, double[] lbest, double[] fofpi, Double gbest, int n, int d, int choice)
    {
		int i;
	    int j;
		double fp;
		double fpmin=0;
		for(i = 0;i<n;i++)
		{
            fp = 0.0;
			for(j = 0;j<d;j++)
            {
				xij[i][j] =PSO.valueinab();
				xijcap[i][j] =xij[i][j];
				vij[i][j] =(java.lang.Math.random()%10000)/10000.0;
				//fp+=g(xij[i][j]);
            }
			fp = PSO.computeFunction(choice, d, xij, i);
			fofpi[i] =fp;
			lbest[i] =fofpi[i];
			if(i == 0)
            {
				fpmin = fp;
				gbest= fpmin;
				for(j = 0;j<d;j++)
				{
					gcap[j] =xij[i][j];
				}
            }
			else
            {
				if(fp<fpmin)
                {
					fpmin = fp;
					gbest= fpmin;
					for(j = 0;j<d;j++)
					{
						gcap[j] =xij[i][j];
					}
				}
			}
        }
		return xij;
    }

    public static void show(double[][] xij, double[][] vij, double[][] xijcap, double[] gcap, double[] lbest, double[] fofpi, double gbest, int n, int d)
    {
		int i;
		int j;
		for(i = 0;i<n;i++)
		{
			System.out.print("xij=[");
			for(j = 0;j<d;j++)
			{
				System.out.print(xij[i][j]);
			}
			System.out.print(" ]");
			System.out.print(" f(pi)= "+fofpi[i]);
			System.out.print(" vij=[");
			for(j = 0;j<d;j++)
			{
				System.out.print(vij[i][j]);
			}
			System.out.print(" ]");
			System.out.print(" Lij=[");
			for(j = 0;j<d;j++)
			{
				System.out.print(xijcap[i][j]);
			}
			System.out.print(" ]");
			System.out.println(" lbest= "+lbest[i]);
	    }
		System.out.print("\n\nGlobal best is "+gbest+" and at [ ");
		for(j = 0;j<d;j++)
		{
			System.out.print(gcap[j]);
		}
		System.out.println("]");
		System.out.print("");
    }

    public static void iterate(double[][] xij, double[][] vij, double[][] xijcap, double[] gcap, double[] lbest, double[] fofpi, Double gbest, int n, int d, int choice)
    {
		int i;
		int j;
		int t;
		int r;
		int count;
		double fp;
		double r1;
		double r2;
		double loc;
		double[] wt = new double[10000];
		System.out.println(" How many iterations :");
		t=sc.nextInt();
		for(count = 0;count<t;count++)
		{
			wt[count] = w-((double)w/t)*count;
		}

		for(count = 0;count<t;count++)
		{
			System.out.println("====== ITERATION "+count+" ====== ");
			for(i = 0;i<n;i++)
			{
				fp = 0.0;
				for(j = 0;j<d;j++)
				{
					r = (int)java.lang.Math.random();
					r1 = (r%1000)/1000.0;
					r = (int)java.lang.Math.random();
					r2 = (r%1000)/1000.0;
					//println("r1="+r1 " , r2="+r2);
					vij[i][j] =wt[count]*vij[i][j]+c1 *r1*(xijcap[i][j]-xij[i][j])+c2 *r2*(gcap[j]-xij[i][j]);
					loc = xij[i][j]+vij[i][j];
					if(loc < a)
					{
						loc = a+(a-loc);

		                if(loc > b)
						{
							loc = a+(b-a)*(java.lang.Math.random()%10000)/10000.0;
						}
					}
					else
					{
							if(loc > b)
							{
								loc = loc-(b-a);

								if(loc < a)
								{
									loc = a+(b-a)*(java.lang.Math.random()%10000)/10000.0;
								}
							}
							xij[i][j] =loc;

				            //fp+=g(xij[i][j]);
					}
			        fp = PSO.computeFunction(choice, d, xij, i);
					fofpi[i] =fp;
			        //UPDATE LOCAL BEST
					if(fofpi[i]<lbest[i])
					{
						lbest[i] =fofpi[i];
			            for(j = 0;j<d;j++)
						{
							xijcap[i][j] =xij[i][j];
						}
					}
			        //UPDATE GLOBAL BEST
					if(fofpi[i]< gbest)
					{
						gbest= fofpi[i];
						for(j = 0;j<d;j++)
						{
			                gcap[j] =xij[i][j];
						}
					}
			}
		}
		PSO.show(xij, vij, xijcap, gcap, lbest, fofpi, gbest, n, d);
	}
	}
    public static int mainMenu()
    {
		int choice;
		System.out.println("\n");
		System.out.println("\t\t  1. Chose option 1 for Sphere function");
		System.out.println("\t\t  2. Chose option 2 for Rosenbrock function");
		System.out.println("\t\t  3. Chose option 3 for Goldstein function");
		System.out.println("\t\t  4. Chose option 4 for Schaffer function");
		System.out.println("\t\t  5. Chose option 5 for SchafferF7 function");
		System.out.println("\t\t  6. Chose option 6 for Easom function");
		System.out.println("\t\t  7. Chose option 7 for Bohachevsky function");
		System.out.println("\t\t  8. Chose option 8 for BohachevskyF2 function");
		System.out.println("\t\t  9. Chose option 9 for BohachevskyF3 function");
		System.out.println("\t\t 10. Chose option 10 for Aluffi function");
		System.out.println("\t\t 11. Chose option 11 for Dekker and Aarts function");
		System.out.println("\t\t 12. Chose option 12 for Modified Rosenbrock function");
		System.out.println("\t\t 13. Chose option 13 for Periodic function");
		System.out.println("\t\t 14. Chose option 14 for Shubert function");
		System.out.println("\t\t 15. Chose option 15 for Camel Back function");
		System.out.println("\t\t 16. Chose option 16 for Camel Back 3 function");
		System.out.println("\t\t 17. Chose option 17 for Becker and Lago function");
		System.out.println("\t\t 18. Chose option 18 for Schwefel function");
		System.out.println("\t\t 19. Chose option 19 for Griewank function");
		System.out.println("\t\t 20. Chose option 20 for Ackley's function");
		System.out.println("\t\t 21. Chose option 21 for Rastrigin function");
		System.out.println("\t\t 22. Chose option 22 for Salomon function");
		System.out.println("\t\t 23. Chose option 23 for Kowalik function");
		System.out.println("\t\t 24. Chose option 24 for Levy and Montalvo function");
		System.out.println("\t\t 25. Chose option 25 for Levy and Montalvo 2 function");
		System.out.println("\t\t 26. Chose option 26 for Meyer and Roth function");
		System.out.println("\t\t 27. Chose option 27 for Miele and Cantrell function");
		System.out.println("\t\t 28. Chose option 28 for Neumaier 2 function");
		System.out.println("\t\t 29. Chose option 29 for Paviani function");
		System.out.println("\t\t 30. Chose option 30 for Powell's Quadratic function");
		System.out.println("\t\t 31. Chose option 31 for Shekel 5 function");
		System.out.println("\t\t 32. Chose option 32 for Shekel 7 function");
		System.out.println("\t\t 33. Chose option 33 for Shekel 10 function");
		System.out.println("\t\t 34. Chose option 34 for wood's or Colville's function");
		//println("\t\t 35. Chose option 35 for *********** function");



		System.out.println("\t\t 99. Chose option 99 to Exit");
		System.out.println(" Choose your option for function ..............");
		choice=sc.nextInt();
		switch(choice)
		{
			case 1:
						break;
			case 2:
						break;
			case 3:
						break;
			case 4:
						break;
			case 5:
						break;
			case 6:
				        break;
		    case 7:
						break;
		    case 8:
						break;
		    case 9:
						break;
		    case 10:
							break;
		    case 11:
							break;
		    case 12:
							break;
		    case 13:
							break;
		    case 14:
							break;
		    case 15:
							break;
		    case 16:
							break;
		    case 17:
							break;
		    case 18:
							break;
		    case 19:
							break;
		    case 20:
							break;
		    case 21:
							break;
			case 22:
							break;
			case 23:
							break;
		    case 24:
							break;
		    case 25:
					        break;
		    case 26:
							break;
		    case 27:
					        break;
		    case 28:
							break;
		    case 29:
							break;
		    case 30:
							break;
		    case 31:
							break;
		    case 32:
							break;
		    case 33:
							break;
		    case 34:
							break;
			case 99:
							System.out.println("\t\t............ Thank You for using our services");
					        System.exit(0);
			default:
							System.out.println("\t.... Wrong choice, please try again");
					       choice = PSO.mainMenu();
							break;
		}
		return choice;
    }

    public static double computeFunction(int choice, int d, double[][] xij, int i)
    {
		int j;
		int k;
		double fofx=0;
	    double temp;
		double temp1;
	    double[][] hartman = {{1,1,3,10,30,0.3689,0.117,0.2673},{2,1.2,0.1,10,35,0.4699,0.4387,0.747},{3,3,3,10,30,0.1091,0.8732,0.5547},{4,3.2,0.1,10,35,0.03815,0.05743,0.8828}};
		double[][] kowalik_data = {{0.1957,0.1947,0.1735,0.16,0.0844,0.0627,0.0456,0.0342,0.0323,0.0235,0.0246},{0.25,0.50,1.0,2.0,4.0,6.0,8.0,10.0,12.0,14.0,16.0}};

	    double[][] meyerroth_data = {{1.0,1.0,0.126},{2.0,1.0,0.219},{1.0,2.0,0.076},{2.0,2.0,0.126},{0.1,0.0,0.186}};
		double[][] shekel_data = {{4,4,4,4,0.1},{1,1,1,1,0.2},{8,8,8,8,0.2},{6,6,6,6,0.4},{3,7,3,7,0.4},{2,9,2,9,0.6},{5,5,3,3,0.3},{8,1,8,1,0.7},{6,2,6,2,0.5},{7,3.6,7,3.6,0.5}};

	    float[] b_Neumaier = {8,18,44,114};

		float[] yofLM = new float[25];
		// ****** Do't run any loop on i as i is for particle number, use j & k
		switch(choice)
		{
			case 1:
						//println("sphere funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d;j++)
				        {
							fofx+=(xij[i][j]*xij[i][j]);
						}
			            break;
	        case 2:
				        //println("Rosenbrock funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d-1;j++)
				        {
							fofx+=((1-xij[i][j])*(1-xij[i][j])+100*(xij[i][j+1]-xij[i][j]*xij[i][j])*(xij[i][j+1]-xij[i][j]*xij[i][j]));
				        }
			            break;
			case 3:
						//println("Rosenbrock funtion chosen");
					    fofx = 0.0;
						for(j = 0;j<d-1;j++)
						{
							fofx+=((1+(xij[i][j]+xij[i][j+1]+1)*(xij[i][j]+xij[i][j+1]+1)*(19-14 *xij[i][j]+3 *xij[i][j]*xij[i][j]-14 *xij[i][j+1]+6 *xij[i][j]*xij[i][j+1]+3 *xij[i][j+1]*xij[i][j+1]))*(30+(2 *xij[i][j]-3 *xij[i][j+1])*(2 *xij[i][j]-3 *xij[i][j+1])*(18-32 *xij[i][j]+12 *xij[i][j]*xij[i][j]+48 *xij[i][j+1]-36 *xij[i][j]*xij[i][j+1]+27 *xij[i][j+1]*xij[i][j+1])));
						}
						break;
	        case 4:
						//println("Rosenbrock funtion chosen");
			            fofx = 0.0;
			            for(j = 0;j<d-1;j++)
				        {
			                fofx+=(0.5+((Math.sin(Math.sqrt(xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1])))*(Math.sin(Math.sqrt(xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1])))-0.5)/((1.0+0.001*(xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1]))*(1.0+0.001*(xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1]))));
				        }
				        break;
        case 5:
		            //println("Rosenbrock funtion chosen");
				    fofx = 0.0;
					for(j = 0;j<d-1;j++)
					{
						fofx+=(Math.pow((xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1]),0.25)*(Math.sin(50*(Math.pow((xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1]),0.1)))*Math.sin(50*(Math.pow((xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1]),0.1)))+1.0));
			        }
					break;
        case 6:
		            //println("Rosenbrock funtion chosen");
				    fofx = 0.0;
					for(j = 0;j<d-1;j++)
					{
						fofx+=(-Math.cos(xij[i][j])*Math.cos(xij[i][j+1])*Math.exp(-(xij[i][j]-22/7.0)*(xij[i][j]-22/7.0)-(xij[i][j+1]-22/7.0)*(xij[i][j+1]-22/7.0)));
					}
					break;
        case 7:
					//println("Rosenbrock funtion chosen");
					fofx = 0.0;
					for(j = 0;j<d-1;j++)
					{
						fofx+=(xij[i][j]*xij[i][j]+2 *xij[i][j+1]*xij[i][j+1]-0.3 *Math.cos(3*(22/7.0)*xij[i][j])-0.4 *Math.cos(4*(22/7.0)*xij[i][j+1])+0.7);
					}
		            break;
		case 8:
					//println("Rosenbrock funtion chosen");
		            fofx = 0.0;
				    for(j = 0;j<d-1;j++)
					{
						fofx+=(xij[i][j]*xij[i][j]+2 *xij[i][j+1]*xij[i][j+1]-(0.3 *Math.cos(3*(22/7.0)*xij[i][j]))*(Math.cos(4*(22/7.0)*xij[i][j+1]))+0.3);
					}
					break;
        case 9:
		            //println("Rosenbrock funtion chosen");
				    fofx = 0.0;
					for(j = 0;j<d-1;j++)
					{
						fofx+=(xij[i][j]*xij[i][j]+2 *xij[i][j+1]*xij[i][j+1]-(0.3 *Math.cos(3*(22/7.0)*xij[i][j]))+(Math.cos(4*(22/7.0)*xij[i][j+1]))+0.3);
					}
					break;
        case 10:
						//println("Rosenbrock funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d-1;j++)
						{
			                fofx+=(0.25 *Math.pow(xij[i][j],4)-0.5 *xij[i][j]*xij[i][j]+0.1 *xij[i][j]+0.5 *xij[i][j+1]*xij[i][j+1]);
						}
			            break;
        case 11:
						//println("Rosenbrock funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d-1;j++)
				        {
							fofx+=(Math.pow(10,5)*xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1]-(xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1])*(xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1])+Math.pow(10,-5)*(Math.pow((xij[i][j]*xij[i][j]+xij[i][j+1]*xij[i][j+1]),4)));
						}
			            break;
        case 12:
						//println("Rosenbrock funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d-1;j++)
				        {
							fofx+=(100*(xij[i][j+1]-xij[i][j]*xij[i][j])*(xij[i][j+1]-xij[i][j]*xij[i][j])+(6.4*(xij[i][j+1]-0.5)*(xij[i][j+1]-0.5)-xij[i][j]-0.6)*(6.4*(xij[i][j+1]-0.5)*(xij[i][j+1]-0.5)-xij[i][j]-0.6));
						}
						break;
        case 13:
			            //println("Rosenbrock funtion chosen");
						fofx = 0.0;
			            for(j = 0;j<d-1;j++)
						{
							fofx+=(1+Math.sin(xij[i][j])*Math.sin(xij[i][j])+Math.sin(xij[i][j+1])*Math.sin(xij[i][j+1])-0.1 *Math.exp(-xij[i][j]*xij[i][j]-xij[i][j+1]*xij[i][j+1]));
						}
			            break;
        case 14:
						//println("Rosenbrock funtion chosen");
			            fofx = 1;
						for(j = 0;j<d;j++)
			            {
							temp = 0.0;
				            for(k = 1;k<=5;k++)
							{
				                temp+=(k *Math.cos((k+1)*xij[i][j]+k));
							}
				            fofx*=temp;
						}
			            break;
        case 15:
						//println("Rosenbrock funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d-1;j++)
				        {
							fofx+=(4 *xij[i][j]*xij[i][j]-2.1 *Math.pow(xij[i][j],4)+Math.pow(xij[i][j],6)/3.0+xij[i][j]*xij[i][j+1]-4 *xij[i][j+1]*xij[i][j+1]+4 *Math.pow(xij[i][j+1],4));
				        }
						break;
        case 16:
			            //println("Rosenbrock funtion chosen");
						fofx = 0.0;
			            for(j = 0;j<d-1;j++)
						{
							fofx+=(2 *xij[i][j]*xij[i][j]-1.05 *Math.pow(xij[i][j],4)+Math.pow(xij[i][j],6)/6.0+xij[i][j]*xij[i][j+1]+xij[i][j+1]*xij[i][j+1]);
						}
			            break;
        case 17:
						//println("Rosenbrock funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d-1;j++)
				        {
							fofx+=((xij[i][j]-5)*(xij[i][j]-5)+(xij[i][j+1]-5)*(xij[i][j+1]-5));
				        }
			            break;
        case 18:
						//println("Rosenbrock funtion chosen");
			            fofx = 0.0;
						for(j = 0;j<d;j++)
				        {
							fofx+=(-xij[i][j]*Math.sin(Math.sqrt(Math.abs(xij[i][j]))));
						}
						break;
        case 19:
						//println("Gre");
			            temp = 0.0;
						for(j = 0;j<d;j++)
				        {
							temp+=xij[i][j]*xij[i][j];
						}
                        fofx = 1+temp/4000.0;
                        temp = 1.0;
                        for(j = 0;j<d;j++)
                        {
                            temp*=Math.cos(xij[i][j]/Math.sqrt(j+1));
                        }
                        fofx = fofx-temp;
                        break;
        case 20:
                        temp = 0.0;
                        for(j = 0;j<d;j++)
                        {
                            temp+=xij[i][j]*xij[i][j];
                        }
                        temp = Math.sqrt(temp/d);
                        temp = -20 *Math.exp(-0.02 *temp);
                        fofx = temp;
                        temp = 0.0;
                        for(j = 0;j<d;j++)
                        {
                            temp+=Math.cos((44.0 *xij[i][j])/7.0);
                        }
                        temp = Math.exp(temp/d);
                        fofx = fofx-temp+20+Math.exp(1);
                        break;
        case 21:
                        temp = 0.0;
                        for(j = 0;j<d;j++)
                        {
                            temp+=xij[i][j]*xij[i][j]-10 *Math.cos(44.0 *xij[i][j]/7.0);
                        }
                        fofx = 10 *d+temp;
                        break;
        case 22:
                        temp = 0.0;
                        for(j = 0;j<d;j++)
                        {
                            temp+=xij[i][j]*xij[i][j];
                        }
                        temp = Math.sqrt(temp);
                        fofx = (1-Math.cos(44.0 *temp/7.0)+0.1 *temp);
                        break;
        case 23: //Only for four dimension
                        temp = 0.0;
                        for(k = 0;k<11;k++)
                        {
                            temp+=((kowalik_data[0][k]-(xij[i][0]*(1+xij[i][1]*kowalik_data[1][k]))/(1+xij[i][2]*kowalik_data[1][k]+xij[i][3]*kowalik_data[1][k]*kowalik_data[1][k]))*(kowalik_data[0][k]-(xij[i][0]*(1+xij[i][1]*kowalik_data[1][k]))/(1+xij[i][2]*kowalik_data[1][k]+xij[i][3]*kowalik_data[1][k]*kowalik_data[1][k])));
                        }
                        fofx = temp;
                        break;
        case 24:
                        for(j = 0;j<d;j++)
                        {
                            yofLM[j] =(float) (1+0.25*(xij[i][j]+1));
                        }
                        temp = 0.0;
                        for(j = 0;j<d-1;j++)
                        {
                            temp+=((yofLM[j]-1)*(yofLM[j]-1)*(1+10 *Math.sin(22.0 *yofLM[j+1]/7.0)*Math.sin(22.0 *yofLM[j+1]/7.0)));
                        }
                        fofx = (22.0/(7.0 *d))*(10 *Math.sin(22.0 *yofLM[0]/7.0)*Math.sin(22.0 *yofLM[0]/7.0)+temp+(yofLM[d-1]-1)*(yofLM[d-1]-1));
                        break;
        case 25:
                        temp = 0.0;
                        for(j = 0;j<d-1;j++)
                        {
                            temp+=((xij[i][j]-1)*(xij[i][j]-1)*(1+Math.sin(66.0 *xij[i][j+1]/7.0)*Math.sin(66.0 *xij[i][j+1]/7.0)));
                        }
                        fofx = (0.1)*(Math.sin(66.0 *xij[i][0]/7.0)*Math.sin(66.0 *xij[i][0]/7.0)+temp+(xij[i][d-1]-1)*(xij[i][d-1]-1)*(1+Math.sin(44.0 *xij[i][d-1]/7.0)*Math.sin(44.0 *xij[i][d-1]/7.0)));
                        break;
        case 26: //Fixed for dimension 3
                        temp = 0.0;
                        for(k = 0;k<5;k++)
                        {
                            temp+=(((xij[i][0]*xij[i][2]*meyerroth_data[k][0])/(1+xij[i][0]*meyerroth_data[k][0]+xij[i][1]*meyerroth_data[k][1])-meyerroth_data[k][2])*((xij[i][0]*xij[i][2]*meyerroth_data[k][0])/(1+xij[i][0]*meyerroth_data[k][0]+xij[i][1]*meyerroth_data[k][1])-meyerroth_data[k][2]));
                        }
                        fofx = temp;
                        break;

        case 27:
                    fofx = (Math.pow((Math.exp(xij[i][0])-xij[i][1]),4)+100 *Math.pow((xij[i][1]-xij[i][2]),6)+Math.pow((Math.tan(xij[i][2]-xij[i][3])),4)+Math.pow(xij[i][0],8));
                    break;
        case 28:
                    temp1 = 0.0;
                    for(k = 0;k<d;k++)
                    {
                        temp = 0.0;
                        for(j = 0;j<d;j++)
                        {
                            temp+=Math.pow(xij[i][j],(k+1));
                        }
                        temp1+=(b_Neumaier[k]-temp)*(b_Neumaier[k]-temp);
                    }
                    fofx = temp1;
                    break;
        case 29:
                    temp1 = 0.0;
                    for(j = 0;j<10;j++)
                    {
                        temp1+=(Math.log(xij[i][j]-2)*Math.log(xij[i][j]-2)+Math.log(10-xij[i][j])*Math.log(10-xij[i][j]));
                    }
                    temp = 1.0;
                    for(j = 0;j<10;j++)
                    {
                        temp*=xij[i][j];
                    }
                    temp = Math.pow(temp,0.2);
                    fofx = temp1-temp;
                    break;
        case 30:
                    fofx = (Math.pow((xij[i][0]+10 *xij[i][0]),2)+5 *Math.pow((xij[i][2]-xij[i][3]),2)+Math.pow((xij[i][1]-2 *xij[i][2]),4)+10 *Math.pow((xij[i][0]-xij[i][3]),4));
                    break;
        case 31:
                    temp1 = 0.0;
                    for(k = 0;k<5;k++)
                    {
                        temp = 0.0;
                        for(j = 0;j<4;j++)
                        {
                            temp+=(xij[i][j]-shekel_data[k][j])*(xij[i][j]-shekel_data[k][j]);
                        }
                        temp+=shekel_data[k][4];
                        temp1+=1/temp;
                    }
                    fofx = -temp1;
                    break;
        case 32:
                    temp1 = 0.0;
                    for(k = 0;k<7;k++)
                    {
                        temp = 0.0;
                        for(j = 0;j<4;j++)
                        {
                            temp+=(xij[i][j]-shekel_data[k][j])*(xij[i][j]-shekel_data[k][j]);
                        }
                        temp+=shekel_data[k][4];
                        temp1+=1/temp;
                    }
                    fofx = -temp1;
                    break;
        case 33:
                    temp1 = 0.0;
                    for(k = 0;k<10;k++)
                    {
                        temp = 0.0;
                        for(j = 0;j<4;j++)
                        {
                            temp+=(xij[i][j]-shekel_data[k][j])*(xij[i][j]-shekel_data[k][j]);
                        }
                        temp+=shekel_data[k][4];
                        temp1+=1/temp;
                    }
                    fofx = -temp1;
                    break;
        case 34: //fixed for 4 dimensions
                    fofx = 100 *Math.pow((xij[i][1]-xij[i][0]*xij[i][0]),2)+Math.pow((1-xij[i][0]),2)+90 *Math.pow((xij[i][3]-xij[i][2]*xij[i][2]),2)+Math.pow((1-xij[i][2]),2)+10.1*(Math.pow((xij[i][1]-1),2)+Math.pow((xij[i][3]-1),2))+19.8*(xij[i][1]-1)*(xij[i][3]-1);
                    break;



		}
		return fofx;
	}
    public static double valueinab()
    {
		//println("Inside valueinab a="+a " ,b="+b);
	    return a+(b-a)*((java.lang.Math.random()%10000)/10000.0);
    }







	public static Matrix returnW1(int choice,int n,int d) 
    {
		int i,j;
		float val=0;
	    double[][] xij = new double[MAX][MAX];
		double[][] xijcap = new double[MAX][MAX];
		double[] gcap = new double[MAX];
		double[][] vij = new double[MAX][MAX];
		double[] fofpi = new double[MAX];
		double gbest=0;
		double[] lbest = new double[MAX];

		Matrix M=new Matrix(n,d);
		double W1[][]=PSO.intialize(xij, vij, xijcap, gcap, lbest, fofpi, gbest, n, d, choice);
		

			for (i=0;i<n ; i++)
			{
				for (j=0;j<d ;j++ )
				{
					val=(float)W1[i][j];
					M.set(i,j,val);
				}
			}
		   
		return M;
    }



















    /*public static void main(String[] args) 
    {
		int i;
	    int n;
	    int d;
		int choice;
	    double[][] xij = new double[MAX][MAX];
		double[][] xijcap = new double[MAX][MAX];
		double[] gcap = new double[MAX];
		double[][] vij = new double[MAX][MAX];
		double[] fofpi = new double[MAX];
		double gbest=0;
		double[] lbest = new double[MAX];
		while (true)
		{
			choice = PSO.mainMenu();
		    System.out.print("\nHow many dimesions ");
		    d=sc.nextInt();
		    System.out.print("\nHow many particles ");
		    n=sc.nextInt();
		    System.out.println("\n"+n+" particle each with "+d+" dimesions  \n");
		    System.out.println("\nINITIALIZATION BEGINS");
			Double tempRef_gbest = new Double(gbest);
		    PSO.intialize(xij, vij, xijcap, gcap, lbest, fofpi, tempRef_gbest, n, d, choice);
		    gbest = tempRef_gbest;
		    System.out.println("\nDISPLAY JUST AFTER INTIALIZATION IS:\n");
		    PSO.show(xij, vij, xijcap, gcap, lbest, fofpi, gbest, n, d);
			Double tempRef_gbest2 = new Double(gbest);
		    PSO.iterate(xij, vij, xijcap, gcap, lbest, fofpi, tempRef_gbest2, n, d, choice);
		    gbest = tempRef_gbest2;
		}
    }*/
}